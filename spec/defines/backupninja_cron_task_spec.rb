require 'spec_helper'

describe 'backupninja::cron::task' do
  context 'supported operating systems' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) { facts }

        let(:title ) { 'backuptask' }

        let(:params) { {
          :file => '/etc/cron.d/backuptask',
          :min  => '45',
          :hour => '4',
        } }

        let(:pre_condition) { 'include ::backupninja' }

        it { is_expected.to compile.with_all_deps }

        it {
          is_expected.to contain_file('/etc/cron.d/backuptask').with(
            :owner => 'root',
            :group => 'root',
            :mode  => '0644',
          )
        }

        context 'with defaults' do
          it {
            is_expected.to contain_file(params[:file]).with_content(
              %r{^45 4 \* \* \* root if \[ -x /usr/sbin/backupninja \]; then /usr/sbin/backupninja; fi$}
            )
          }
        end

        context 'with custom command set' do
          let(:params) do
            super().merge({ :command =>  '/usr/local/sbin/backupninja' })
          end

          it {
            is_expected.to contain_file(params[:file]).with_content(
              %r{^45 4 \* \* \* root if \[ -x /usr/sbin/backupninja \]; then /usr/local/sbin/backupninja; fi$}
            )
          }
        end

        context 'with custom command_options set' do
          let(:params) do
            super().merge({ :command_options =>  '--debug' })
          end

          it {
            is_expected.to contain_file(params[:file]).with_content(
              %r{^45 4 \* \* \* root if \[ -x /usr/sbin/backupninja \]; then /usr/sbin/backupninja --debug; fi$}
            )
          }
        end

        context 'with custom test_command set' do
          let(:params) do
            super().merge({ :test_command =>  '/bin/true' })
          end

          it {
            is_expected.to contain_file(params[:file]).with_content(
              %r{^45 4 \* \* \* root if /bin/true; then /usr/sbin/backupninja; fi$}
            )
          }
        end
 
        context 'with ensure set to absent' do
          let(:params) do
              super().merge({ :ensure =>  'absent' })
          end

          it { is_expected.to contain_file(params[:file]).with_ensure('absent') }
        end

      end
    end
  end
end
