require 'spec_helper'

describe 'backupninja::config_setting' do
  it { is_expected.to run.with_params('foo', 123).and_return("foo = 123\n") }
  it { is_expected.to run.with_params('foo', 'bar').and_return("foo = bar\n") }
  it { is_expected.to run.with_params('foo', false).and_return("foo = no\n") }
  it { is_expected.to run.with_params('foo', ['bar', 'baz']).and_return("foo = bar\nfoo = baz\n") }
  it { is_expected.to run.with_params('foo', :undef).and_return(nil) }
end
