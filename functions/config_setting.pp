# Transforms and key/value pair into a backupninja configuration setting
#
# @param key The configuration key.
#
# @param value The value to assign to the key.
#
# @return Returns a string to insert in a backupninja configuration file.
#
# @example backupninja::config_setting('reportsuccess', true)
#
function backupninja::config_setting( String $key, Data $value ) >> Optional[String] {
  $line = case $value {
    Array[String, 1]: { join($value.map |$v| { "${key} = ${v}" }, "\n") }
    Boolean: { "${key} = ${bool2str($value, 'yes', 'no')}" }
    Scalar: { "${key} = ${value}" }
    default: { undef }
  }
  if $line {
    "${line}\n"
  }
}
