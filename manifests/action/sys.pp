# Backup important system information, as part of a backupninja run.
#
define backupninja::action::sys (
  String $ensure                 = 'present',
  Integer $order                 = 30,
  Optional[String] $when         = undef,
  Optional[Boolean] $validate    = undef,
  # backup config
  String $parentdir              = '/var/backups',
  String $packagesfile           = '/var/backups/dpkg-selections.txt',
  String $selectionsfile         = '/var/backups/debconfsel.txt',
  String $partitionsfile         = '/var/backups/partitions.__star__.txt',
  String $hardwarefile           = '/var/backups/hardware.txt',
  String $luksheadersfile        = '/var/backups/luksheader.__star__.bin',
  Boolean $packages              = true,
  Boolean $partitions            = true,
  Boolean $dosfdisk              = true,
  Boolean $hardware              = true,
  Boolean $dohwinfo              = true,
  Boolean $luksheaders           = false,
  Boolean $lvm                   = false,
  Boolean $mbr                   = false,
  Boolean $bios                  = false,
) {

  if $backupninja::manage_packages {
    if $packages == true and $backupninja::selections_package_name {
      ensure_packages([$backupninja::selections_package_name], {'ensure' => 'installed'})
    }
    if $dohwinfo == true and $backupninja::hwinfo_package_name {
      ensure_packages([$backupninja::hwinfo_package_name], {'ensure' => 'installed'})
    }
    if $dosfdisk == true and $backupninja::sfdisk_package_name {
      ensure_packages([$backupninja::sfdisk_package_name], {'ensure' => 'installed'})
    }
    if $luksheaders == true and $backupninja::cryptsetup_package_name {
      ensure_packages([$backupninja::cryptsetup_package_name], {'ensure' => 'installed'})
    }
    if $bios == true and $backupninja::flashrom_package_name {
      ensure_packages([$backupninja::flashrom_package_name], {'ensure' => 'installed'})
    }
  }

  $config = {
    'when'            => $when,
    'parentdir'       => $parentdir,
    'packagesfile'    => $packagesfile,
    'selectionsfile'  => $selectionsfile,
    'partitionsfile'  => $partitionsfile,
    'hardwarefile'    => $hardwarefile,
    'luksheadersfile' => $luksheadersfile,
    'packages'        => $packages,
    'partitions'      => $partitions,
    'dosfdisk'        => $dosfdisk,
    'hardware'        => $hardware,
    'dohwinfo'        => $dohwinfo,
    'luksheaders'     => $luksheaders,
    'lvm'             => $lvm,
    'mbr'             => $mbr,
    'bios'            => $bios,
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'sys',
    content  => epp('backupninja/action.epp', { 'config' => $config }),
    validate => $validate,
  }

}
